script generously created by :https://github.com/mislav/ssl-tools/blob/master/doctor.rb

If your Ruby version is compiling a broken version of openSSL or you'd like to get some performance metrics,
his script is useful for identifying internal configurations along with connecting to a website over HTTPS. 
